// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

import problem4 from  "../solutions/problem4.js"

function olderThan2000(params) {
    let years = problem4.printCarYear(params)
    // console.log(years);
    const before2000 = []
    for(let year of years){
        if(year < 2000){
            before2000.push(year)
        }
    }
    return before2000;
}

function filter2000(params) {
    let carYears = problem4.printCarYear(params)
    return carYears.filter((year) => {
        return year < 2000;
    })
}



export default { olderThan2000, filter2000 };