// The marketing team wants the car models listed alphabetically on the website. 
// Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
function sortByModel(inventoryData) {

    const n = inventoryData.length;
    let swapped;

    do {
        swapped = false;

        for (let i = 0; i < n - 1; i++) {
            if (inventoryData[i].car_model > inventoryData[i + 1].car_model) {
                const temp = inventoryData[i];
                inventoryData[i] = inventoryData[i + 1];
                inventoryData[i + 1] = temp;
                swapped = true;
            }
        }
    } while (swapped);

    return inventoryData;

}


function modelSort(inventoryData) {
    inventoryData.sort((a,b)=>{
        return a.car_model.localeCompare(b.car_model)
    })
    return inventoryData
}

export default { sortByModel, modelSort };