// {


// used to take input from user in command line 
// import { createInterface } from 'readline';

// const rl = createInterface({
//   input: process.stdin,
//   output: process.stdout
// });

// rl.question('Please enter something: ', (userInput) => {
//   console.log('You entered: ' + userInput);
//   rl.close();
// });

// }

function getDetails(inventoryData) {
    for(let details of inventoryData){
        if(details["id"] == 33){
            console.log(`Car 33 is a ${details["car_year"]} ${details["car_make"]} ${details["car_model"]}`)
            break;
        }
    }
}

function getDetails2(inventoryData) {
    const found = inventoryData.filter((element) => {
        if(element["id"] == 33){
            // console.log(`Car 33 is a ${element["car_year"]} ${element["car_make"]} ${element["car_model"]}`)
            return element
        }
    })
    return found;
}


// console.log(getDetails2(inventory));

export default {getDetails, getDetails2};