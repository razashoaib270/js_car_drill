// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function bmw_audi(inventoryData) {
    let result = [];
    for(let data of inventoryData){
        if (data.car_make === 'BMW' || data.car_make === 'Audi'){
            result.push(data);
        }
        // console.log(data.car_make);
    }
    return result;
}

function onlyBMW_AUDI(inventoryData) {
    let result = []
    inventoryData.filter((ele) => {
        if ((ele.car_make === 'BMW' || ele.car_make === 'Audi')){
            result.push(ele);
        }
    })
    return result;
}

export default { bmw_audi, onlyBMW_AUDI };