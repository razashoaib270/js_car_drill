// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.


function printCarYear(inventoryData) {
    let year_data = []
    for(let car of inventoryData){
        year_data.push(car.car_year)
    }
    return year_data
}


function getCarYear(inventoryData) {
    let year_data = []
    inventoryData.map((ele) => {
        year_data.push(ele.car_year)
    })
    return year_data
}

export default { printCarYear, getCarYear};




// module.exports = printCarYear    // commonJs Format(cjs)
