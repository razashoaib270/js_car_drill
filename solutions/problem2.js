function getDetails(inventoryData) {

    const lastCarDetails = inventoryData[inventoryData.length-1]
    console.log(`Last car is a ${lastCarDetails["car_make"]} ${lastCarDetails["car_model"]}`)
    
}

function getLast(inventoryData) {
    const last = inventoryData.filter((detail, index, array) => {
        if (index === array.length - 1){
            return detail;
        }
    } )
    return last;
}


export default {getDetails, getLast };