const sampleData =[{"id":33,"car_make":"Honda","car_model":"Verna","car_year":2023},
{"id":17,"car_make":"Buick","car_model":"Skylark","car_year":1987},
{"id":37,"car_make":"Oldsmobile","car_model":"LSS","car_year":1997},
{"id":25,"car_make":"BMW","car_model":"525","car_year":2005},
{"id":8,"car_make":"Audi","car_model":"4000CS Quattro","car_year":1987},
]

export default sampleData