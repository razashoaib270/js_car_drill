import problem5 from '../solutions/problem5.js';
import inventory from '../inventory/inventory.js';
import sampleData from './sampleData.js'

const res = problem5.olderThan2000(inventory)
console.log(res.length);

const sample = problem5.olderThan2000(sampleData)
console.log(sample.length);

// js drill 2

const res2 = problem5.filter2000(inventory)
console.log(res2.length);

const sample2 = problem5.filter2000(sampleData);
console.log(sample2.length);