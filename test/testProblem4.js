import problem4 from '../solutions/problem4.js';
import inventory from '../inventory/inventory.js';
import sampleData from './sampleData.js'


const res = problem4.printCarYear(inventory)
console.log(res);

const sample = problem4.printCarYear(sampleData)
console.log(sample);


// js car drill 2

const drill2 = problem4.getCarYear(inventory)
console.log(drill2);